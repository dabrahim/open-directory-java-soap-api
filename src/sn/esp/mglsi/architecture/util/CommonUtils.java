package sn.esp.mglsi.architecture.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Random;

public class CommonUtils {

    private CommonUtils() {
    }

    public static String hashPassword(String password) {
        return DigestUtils.sha256Hex(password);
    }

    public static String generateRandomString(int length) {
        final String CHAR_SET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRTUVWXYZ123456789";
        final int CHAR_SET_LENGTH = CHAR_SET.length();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < length; i++) {

            stringBuilder.append(
                    CHAR_SET.charAt((new Random()).nextInt(CHAR_SET_LENGTH))
            );
        }

        return stringBuilder.toString();
    }
}
