package sn.esp.mglsi.architecture.util;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class RegistrationSMSSender implements Runnable {
    private static final String ACCOUNT_SID = "ACacba127c1d2d0e682c4b4b375373774a";
    private static final String AUTH_TOKEN = "c745ba971c34401cad275a52667bc1d5";

    private String password;

    public RegistrationSMSSender(String password) {
        this.password = password;
    }

    private static String sendSMS(String phone, String content) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message
                .creator(new PhoneNumber(phone), // to
                        new PhoneNumber("+12055767572"), // from
                        content)
                .create();

        return message.getSid();
    }

    public static String sendTemporaryPassword(String phone, String password) {
        return RegistrationSMSSender
                .sendSMS("+221778140352",
                        String.format("Welcome, your temporary password is : %s", password));
    }

    @Override
    public void run() {
        RegistrationSMSSender.sendTemporaryPassword("+221778140352", password);
    }
}
