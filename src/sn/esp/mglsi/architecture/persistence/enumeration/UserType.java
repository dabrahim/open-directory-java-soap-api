package sn.esp.mglsi.architecture.persistence.enumeration;

public enum UserType {
    ADMIN,
    REGULAR
}
