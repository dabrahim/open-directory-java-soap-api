package sn.esp.mglsi.architecture.persistence.repository;

import java.sql.*;
import java.util.ArrayList;


public class Database {
    private Connection connection;
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private String hostName;
    private String dbName;
    private String login;
    private String password;


    public Database(String hostName, String dbName, String userName, String password) throws SQLException {
        this.hostName = hostName;
        this.dbName = dbName;
        this.login = userName;
        this.password = password;

        try {
            Class.forName(JDBC_DRIVER);
            connection = getConnection(hostName, dbName, userName, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            if (connection.isClosed()) {
                connection = getConnection(this.hostName, this.dbName, this.login, this.password);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    private static Connection getConnection(String hostName, String dbName, String login, String password) throws SQLException {
        return DriverManager
                .getConnection(String.format("jdbc:mysql://%s/%s?autoReconnect=true&useSSL=false", hostName, dbName), login, password);
    }

    public ResultSet query(String queryStatement) throws SQLException {
        return connection.createStatement().executeQuery(queryStatement);
    }

    public static String[][] formatResultSet(ResultSet resultSet) throws SQLException {
        // WE START BY COUNTING DOWN THE NUMBER OF COLUMNS
        final int COLUMN_COUNT = resultSet.getMetaData().getColumnCount();
        ArrayList<String[]> result = new ArrayList<>();

        // WE LOOP THROUGH THE RESULTSET AND STORE EACH RESULT INTO THE ARRAYLIST
        // OF STRING ARRAYS
        while (resultSet.next()) {
            String[] row = new String[COLUMN_COUNT];

            for (int i = 0; i < row.length; i++) {
                row[i] = resultSet.getString(i + 1);
            }

            result.add(row);
        }

        // WE COUNT DOWN THE NUMBER OF ROWS THAN WE CREATE A TWO DIMENTIONNAL STRING ARRAY
        // IN WICH WE STORE THE DATA IN THE DESIRED FORMAT
        final int ROW_COUNT = result.size();
        String[][] data = new String[ROW_COUNT][COLUMN_COUNT];

        for (int i = 0; i < ROW_COUNT; i++) {
            data[i] = result.get(i);
        }

        return data;
    }

    public void execute(String updateStatement) throws SQLException {
        connection.createStatement().executeUpdate(updateStatement);
    }

    public int execute(String updateStatement, int options) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate(updateStatement, options);

        ResultSet rs = statement.getGeneratedKeys();
        if (rs != null && rs.next()) {
            return rs.getInt(1);
        } else {
            return 0;
        }
    }

    public PreparedStatement preparedStatement(String statement) throws SQLException {
        return connection.prepareStatement(statement);
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
    }

    public void setAutoCommit(boolean choice) throws SQLException {
        connection.setAutoCommit(choice);
    }

    public void commit() throws SQLException {
        connection.commit();
    }

    public void rollback() throws SQLException {
        connection.rollback();
    }

}
	