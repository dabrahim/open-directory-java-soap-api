package sn.esp.mglsi.architecture.persistence.repository;

import sn.esp.mglsi.architecture.persistence.dao.TokenDAO;
import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.Token;

import java.sql.*;

public class TokenRepository extends AbstractRepository implements TokenDAO {
    private static final String SQL_INSERT = "INSERT INTO tokens (user_id, value) VALUES (?,?)";
    private static final String SQL_FIND_BY_ID = "SELECT * FROM tokens WHERE id = ?";
    private static final String SQL_FIND_BY_VALUE = "SELECT * FROM tokens WHERE value = ?";
    private static final String SQL_FIND_BY_USER_ID = "SELECT * FROM tokens WHERE user_id = ?";
    private static final String SQL_DELETE = "DELETE FROM tokens WHERE id = ?";

    @Override
    public Token findByUserId(Long userId) throws DatabaseException {
        Token token = null;

        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_USER_ID)) {

            preparedStatement.setLong(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    token = hydrate(resultSet);
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return token;
    }

    @Override
    public Token findByValue(String value) throws DatabaseException {
        Token token = null;

        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_VALUE)) {

            preparedStatement.setString(1, value);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    token = hydrate(resultSet);
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return token;
    }

    @Override
    public Token findById(Long id) throws DatabaseException {
        Token token = null;

        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            preparedStatement.setLong(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    token = hydrate(resultSet);
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return token;
    }

    private Token hydrate(ResultSet resultSet) throws SQLException {
        Token token = new Token();

        token.setId(resultSet.getLong("id"));
        token.setUserId(resultSet.getLong("user_id"));
        token.setCreationDate(resultSet.getDate("creation_date"));
        token.setValue(resultSet.getString("value"));

        return token;
    }

    @Override
    public Token create(Token token) throws DatabaseException {
        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setLong(1, token.getUserId());
            preparedStatement.setString(2, token.getValue());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DatabaseException("Creating token  failed, no rows affected");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    token = findById(generatedKeys.getLong(1));

                } else {
                    throw new DatabaseException("Creating token failed, no ID obtained");
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return token;
    }

    @Override
    public void remove(Long id) throws DatabaseException {
        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {

            preparedStatement.setLong(1, id);

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DatabaseException("Deleting token failed, no rows affected");
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }
    }
}
