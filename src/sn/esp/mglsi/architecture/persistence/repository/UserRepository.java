package sn.esp.mglsi.architecture.persistence.repository;

import sn.esp.mglsi.architecture.persistence.dao.UserDAO;
import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.User;
import sn.esp.mglsi.architecture.util.CommonUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository implements UserDAO {
    private static final String SQL_INSERT = "INSERT INTO users (first_name, last_name, login, password) VALUES (?, ?, ?, ?)";
    private static final String SQL_FIND_BY_ID = "SELECT * FROM users WHERE id = ?";
    private static final String SQL_FIND_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    private static final String SQL_UPDATE = "UPDATE users SET first_name = ?, last_name = ?, login = ?, password = ? WHERE id = ?";
    private static final String SQL_FIND_ALL = "SELECT * FROM users";
    private static final String SQL_DELETE = "DELETE FROM users WHERE id = ?";
    private static final String SQL_LOGIN = "SELECT 1 FROM users WHERE login = ? AND password = ?";

    @Override
    public boolean areValidCredentials(String login, String password) throws DatabaseException {
        boolean isValid;

        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOGIN)) {

            String hashedPassword = CommonUtils.hashPassword(password);

            preparedStatement.setString(1, login);
            preparedStatement.setString(2, hashedPassword);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                isValid = resultSet.next();
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return isValid;
    }

    @Override
    public User findByLogin(String login) throws DatabaseException {
        User user = null;

        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_LOGIN)) {

            preparedStatement.setString(1, login);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    user = hydrate(resultSet);
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return user;
    }

    @Override
    public User create(User user) throws DatabaseException {
        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            String hashedPassword = CommonUtils.hashPassword(user.getPassword());

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, hashedPassword);

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DatabaseException("Creating user  failed, no rows affected");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user = findById(generatedKeys.getLong(1));

                } else {
                    throw new DatabaseException("Creating user failed, no ID obtained");
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return user;
    }

    @Override
    public User findById(Long id) throws DatabaseException {
        User user = null;

        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            preparedStatement.setLong(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                if (resultSet.next()) {
                    user = hydrate(resultSet);
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return user;
    }

    @Override
    public void delete(Long id) throws DatabaseException {
        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {

            preparedStatement.setLong(1, id);

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DatabaseException("Deleting user  failed, no rows affected");
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }
    }

    @Override
    public User update(User user) throws DatabaseException {
        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {

            String hashedPassword = CommonUtils.hashPassword(user.getPassword());

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, hashedPassword);
            preparedStatement.setLong(5, user.getId());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DatabaseException("Updating user  failed, no rows affected");
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return findById(user.getId());
    }

    @Override
    public List<User> findAll() throws DatabaseException {
        List<User> users = new ArrayList<>();

        try (Connection connection = databaseSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {

            while (resultSet.next()) {
                User user = hydrate(resultSet);
                users.add(user);
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }

        return users;
    }

    private static User hydrate(ResultSet resultSet) throws SQLException {
        User user = new User();

        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setId(resultSet.getLong("id"));
        user.setRegistrationDate(resultSet.getDate("registration_date"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));

        return user;
    }
}
