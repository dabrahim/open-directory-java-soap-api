package sn.esp.mglsi.architecture.persistence.exception;

public class DatabaseException extends Exception {
    public DatabaseException(String message) {
        super(message);
    }
}
