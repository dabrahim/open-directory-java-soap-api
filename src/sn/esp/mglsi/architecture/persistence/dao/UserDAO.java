package sn.esp.mglsi.architecture.persistence.dao;

import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.User;

import java.util.List;

public interface UserDAO {
    User create(User user) throws DatabaseException;
    User findById(Long id) throws DatabaseException;
    void delete(Long id) throws DatabaseException;
    User update(User user) throws DatabaseException;
    List<User> findAll() throws DatabaseException;
    boolean areValidCredentials(String login, String password) throws DatabaseException;
    User findByLogin(String login) throws DatabaseException;
}
