package sn.esp.mglsi.architecture.persistence.dao;

import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.Token;

public interface TokenDAO {
    Token findByUserId(Long userId) throws DatabaseException;
    Token findByValue(String value) throws DatabaseException;
    Token findById(Long id) throws DatabaseException;
    Token create(Token token) throws DatabaseException;
    void remove(Long id) throws DatabaseException;
}
