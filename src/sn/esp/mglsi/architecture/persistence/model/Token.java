package sn.esp.mglsi.architecture.persistence.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Token implements Serializable {
    @XmlTransient
    private Long id;
    @XmlTransient
    private Long userId;
    private String value;
    private Date creationDate;

    public final static int LENGTH = 50;

    public Token(Long id, Long userId, String value, Date creationDate) {
        this.id = id;
        this.userId = userId;
        this.value = value;
        this.creationDate = creationDate;
    }

    public Token() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", userId=" + userId +
                ", value='" + value + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
