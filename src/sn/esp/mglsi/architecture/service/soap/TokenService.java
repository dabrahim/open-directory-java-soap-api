package sn.esp.mglsi.architecture.service.soap;

import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface TokenService {

    @WebMethod(operationName = "retrieveTokenUser")
    User retrieveUser(@WebParam(name = "tokenValue") String tokenValue) throws DatabaseException;
}
