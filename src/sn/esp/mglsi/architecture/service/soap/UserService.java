package sn.esp.mglsi.architecture.service.soap;

import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.Token;
import sn.esp.mglsi.architecture.persistence.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface UserService {

    @WebMethod
    User add(@WebParam(name = "user") User user) throws DatabaseException;

    @WebMethod
    User findById(@WebParam(name = "userId") Long id) throws DatabaseException;

    @WebMethod
    List<User> getAll() throws DatabaseException;

    @WebMethod
    void delete(@WebParam(name = "userId") Long id) throws DatabaseException;

    @WebMethod
    User update(@WebParam(name = "user") User user) throws DatabaseException;

    @WebMethod
    Token login(@WebParam(name = "login") String login, @WebParam(name = "password") String password) throws DatabaseException;
}
