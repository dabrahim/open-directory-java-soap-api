package sn.esp.mglsi.architecture.service.soap;

import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.Token;
import sn.esp.mglsi.architecture.persistence.model.User;
import sn.esp.mglsi.architecture.persistence.repository.TokenRepository;
import sn.esp.mglsi.architecture.persistence.repository.UserRepository;
import sn.esp.mglsi.architecture.util.CommonUtils;
import sn.esp.mglsi.architecture.util.RegistrationSMSSender;

import javax.annotation.PostConstruct;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(name = "UserWS", endpointInterface = "sn.esp.mglsi.architecture.service.soap.UserService")
public class UserServiceImpl implements UserService {

    private UserRepository userRepo;
    private TokenRepository tokenRepo;

    @PostConstruct
    private void createRepository() {
        userRepo = new UserRepository();
        tokenRepo = new TokenRepository();
    }

    @Override
    public User add(@WebParam(name = "user") User user) throws DatabaseException {
        String tmpPassword = CommonUtils.generateRandomString(10);
        user.setPassword(tmpPassword);

        Thread thread = new Thread(new RegistrationSMSSender(tmpPassword));
        thread.start();

        return userRepo.create(user);
    }

    @Override
    public User findById(Long id) throws DatabaseException {
        return userRepo.findById(id);
    }

    @Override
    public List<User> getAll() throws DatabaseException {
        return userRepo.findAll();
    }

    @Override
    public void delete(Long id) throws DatabaseException {
        userRepo.delete(id);
    }

    @Override
    public User update(@WebParam(name = "user") User user) throws DatabaseException {
        return userRepo.update(user);
    }

    @Override
    public Token login(@WebParam(name = "login") String login, @WebParam(name = "password") String password) throws DatabaseException {
        Token token = null;

        if (userRepo.areValidCredentials(login, password)) {
            User user = userRepo.findByLogin(login);

            //We check whether this user already has a token
            token = tokenRepo.findByUserId(user.getId());

            //If no token was found we create a new one
            if (token == null) {
                token = new Token();
                token.setValue(CommonUtils.generateRandomString(Token.LENGTH));
                token.setUserId(user.getId());
                token = tokenRepo.create(token);
            }
        }

        return token;
    }
}
