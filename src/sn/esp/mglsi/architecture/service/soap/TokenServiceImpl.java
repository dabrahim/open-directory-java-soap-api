package sn.esp.mglsi.architecture.service.soap;

import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.Token;
import sn.esp.mglsi.architecture.persistence.model.User;
import sn.esp.mglsi.architecture.persistence.repository.TokenRepository;
import sn.esp.mglsi.architecture.persistence.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.jws.WebService;

@WebService(name = "TokenWS",
        endpointInterface = "sn.esp.mglsi.architecture.service.soap.TokenService")
public class TokenServiceImpl implements TokenService {

    private TokenRepository tokenRepo;
    private UserRepository userRepo;

    @PostConstruct
    public void createRepos() {
        tokenRepo = new TokenRepository();
        userRepo = new UserRepository();
    }

    @Override
    public User retrieveUser(String tokenValue) throws DatabaseException {
        User user = null;
        Token token = tokenRepo.findByValue(tokenValue);

        if (token != null) {
            user = userRepo.findById(token.getUserId());
        }

        return user;
    }
}
