package sn.esp.mglsi.architecture;

import sn.esp.mglsi.architecture.persistence.exception.DatabaseException;
import sn.esp.mglsi.architecture.persistence.model.User;
import sn.esp.mglsi.architecture.persistence.repository.UserRepository;
import sn.esp.mglsi.architecture.service.soap.TokenServiceImpl;
import sn.esp.mglsi.architecture.service.soap.UserServiceImpl;

import javax.xml.ws.Endpoint;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

        String address = "http://localhost:8585/";
        Endpoint.publish(address + "users/", new UserServiceImpl());
        Endpoint.publish(address + "tokens/", new TokenServiceImpl());

        System.out.println(String.format("Server successfully started ! ADDRESS: %s", address));
    }

    private static void loginTest() {
        UserRepository userRepository = new UserRepository();

        try {
            boolean valid = userRepository.areValidCredentials("dabrahim", "131296");
            if (valid) {
                System.out.println("VALID");
                System.out.println(userRepository.findByLogin("dabrahim"));

            } else {
                System.out.println("Login or password incorrect");
            }

        } catch (DatabaseException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void deleteTest() {
        UserRepository userRepository = new UserRepository();

        try {
            userRepository.delete(2L);
            System.out.println("The user was successfully deleted");

        } catch (DatabaseException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void findAllTest() {
        UserRepository userRepository = new UserRepository();

        try {
            List<User> users = userRepository.findAll();

            for (User user : users) {
                System.out.println(user);
            }

        } catch (DatabaseException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void updateTest() {
        User user = new User();
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setLogin("johndoe");
        user.setPassword("131296");
        user.setId(1L);

        UserRepository userRepository = new UserRepository();
        try {
            user = userRepository.update(user);

            System.out.println(user);

        } catch (DatabaseException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void findTest() {
        UserRepository userRepository = new UserRepository();

        try {
            User user = userRepository.findById(1L);
            System.out.println(user);

        } catch (DatabaseException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void createTest() {
        User user = new User();
        user.setFirstName("Modou");
        user.setLastName("Ndiaye");
        user.setLogin("modou");
        user.setPassword("131296");

        UserRepository userRepository = new UserRepository();
        try {
            user = userRepository.create(user);
            System.out.println(user);

        } catch (DatabaseException e) {
            System.err.println(e.getMessage());
        }
    }
}
